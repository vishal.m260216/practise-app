import { Component, Injectable, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { Employee } from '../employee.model';

@Injectable()
@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit{
  public employees:any = [];
  // public errorMsg;
    constructor(private employeeService: EmployeeService) { }
  
    ngOnInit() {
      this.employeeService.getEmployees().subscribe((res)=>{
        this.employees = res;        
      })
      
    }
  }