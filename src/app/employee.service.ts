import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Employee } from './employee.model';
import { Observable } from 'rxjs';


@Injectable()
export class EmployeeService {

  private _url: string = "./assets/data/employees.json";

  constructor(private http:HttpClient) { }

  getEmployees(){
    return this.http.get(this._url);
  }
  // errorHandler(error: HttpErrorResponse){
  //   return Observable.throw(error.message || "Server Error");
  // }

}